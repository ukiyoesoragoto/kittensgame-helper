// ==UserScript==
// @name         kittensgame helper
// @namespace    http://tampermonkey.net/
// @version      20240719.8
// @description  你的喵国你做主
// @author       UkiyoESoragoto
// @match        https://kittensgame.com/beta/
// @icon         https://www.google.com/s2/favicons?sz=64&domain=kittensgame.com
// @require      https://code.jquery.com/jquery-3.6.0.min.js
// @grant        none
// ==/UserScript==

(function () {
  'use strict';

  // Your code here...
  setInterval(function () {
    $('.btnContent .btnTitle:contains("采集猫薄荷")').click();
    // 修改jq方法为js原生方法

  }, 1);

  // 自动狩猎
  // 定时检查元素中的数字

  const autoHunt = () => {
    // 提取数字
    const fastHuntContainerCount = $('#fastHuntContainerCount');
    const catpower = parseFloat($('[data-reactid=".0.0.1.1.b.2"]').text());
    const catpowerMax = parseFloat($('[data-reactid=".0.0.1.1.b.3"]').text().replace('/', ''));

    if (catpowerMax - 100 > catpower) return;
    fastHuntContainerCount.click();
  }

  // 自动赞美太阳
  // 定时检查元素中的数字

  const autoFaith = () => {
    // 提取数字
    const faith = parseFloat($('[data-reactid=".0.0.1.1.e.2"]').text());
    const faithMax = parseFloat($('[data-reactid=".0.0.1.1.e.3"]').text().replace('/', ''));

    if (faithMax - 1 > faith) return;

    // 使用 document.querySelector 来选取具有特定 data-reactid 的 <a> 元素
    const linkElement = document.querySelector('a[data-reactid=".0.3.0"]');

    // 检查元素是否存在
    if (!linkElement) return;
    const clickEvent = new MouseEvent('click', {
      'view': window,
      'bubbles': true,
      'cancelable': false
    });
    linkElement.dispatchEvent(clickEvent);

  }

  // region 自动观察

  const autoObs = () => {
    // 检查元素是否存在
    const observeBtn = $('#observeBtn');
    if (observeBtn.length > 0) {
      // 如果元素存在，点击它
      observeBtn.click();
    }
  };
  setInterval(() => {
    autoObs();
    autoHunt();
    autoFaith();
  }, 2000);

  // endregion

  // region 自动工坊

  // 生成一个按钮开关
  let htmlCtx = '<div style="padding-top:10px">' +
    '<input type="checkbox" id="autoManuscript">' +
    '真·自动工坊 (๑•̀ㅂ•́)و✧<br>' +
    '每秒检查一次，可合成总量大于[玩家预设]时，合成可合成总量的1%<br>' +
    '[可合成总量]可以将鼠标悬停在“全部”按钮上查看<br>' +
    '预设为0则不启用<br>' +
    '由于技术原因（懒），玩家预设值暂不支持保存，每次加载游戏后需要重新填写<br>' +
    '</div>';
  const button = $(htmlCtx);

  const addButton = setInterval(
    () => {
      const obj = $('[data-reactid=".0.5.1"]');
      if (obj.length > 0) {
        // 将新的input元素添加到id为fastHuntContainer的div内的第一个元素
        obj.prepend(button);
        clearInterval(addButton);
      }
    },
    1000,
  );

  const addAutoInput = setInterval(
    () => {
      const wood = $('[data-reactid=".0.5.1.0"]');

      if (wood.length < 1) {
        return;
      }

      // 获取wood的所有子节点
      const children = wood.children();
      // 遍历子节点
      for (let i = 0; i < children.length; i++) {
        const child = children[i];
        // 获取子节点的文本内容
        const text = child.textContent;
        // 获取子节点的id
        const id = child.id;
        // 获取子节点的class
        const className = child.className;
        // 获取子节点的data-reactid
        const dataReactid = child.dataset.reactid;

        // 在所有child的子元素的第一个位置插入一个input元素
        child.insertAdjacentHTML('afterbegin', '<input style="width: 40px;margin:0" value="0">');

        setInterval(
          () => autoCheck(child),
          1000,
        );
      }

      clearInterval(addAutoInput);
    },
    1000,
  )

  const getNum = title => {
    let ret = 0;

    if (title.includes('K')) {
      ret = parseInt(title.replace('K', ''), 10) * 1000;
    } else {
      ret = parseInt(title, 10);
    }

    return ret;
  };

  const autoCheck = (aRow) => {

    const autoManuscript = $('#autoManuscript');
    if (autoManuscript.length < 1) {
      return;
    }

    if (!autoManuscript[0].checked) {
      return;
    }

    const grandChildren = aRow.children;
    if (grandChildren.length <= 0) {
      return;
    }
    const range = getNum(grandChildren[0].value);

    if (range < 1) {
      return;
    }

    const lastChild = grandChildren[grandChildren.length - 1];
    if (lastChild.length <= 0) {
      return;
    }
    let firstChild = lastChild.firstChild;
    if (firstChild.length <= 0) {
      return;
    }
    const max = getNum(firstChild.title.replace('+', ''));

    if (max <= range) {
      return;
    }

    grandChildren[3].firstChild.click();
  }

  // endregion
})();

// 猫薄荷 catnip
// 木材 wood
// 矿物 minerals
// 煤 coal
// 铁 iron
// 钛 titanium
// 黄金 gold
// 石油 oil
// 喵力 catpower
// 科学 science
// 文化 culture
// 信仰 faith
// 猫口 kittens
// 星图 starchart
// 毛皮 furs
// 象牙 ivory
// 香料 spice
// 独角兽 unicorns
// 眼泪 tears
//
// 木材 wood
// 木梁 beam
// 石板 slab
// 金属板 plate
// 钢 steel
// 齿轮 gear
// 脚手架 scaffold
// 船 ship
// 羊皮纸 parchment
// 手稿 manuscript
// 概要 compendium
// 蓝图 blueprint
// 巨石 megalith

